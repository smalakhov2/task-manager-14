package ru.malakhov.tm.api.controller;

public interface IUserController {

    void profile();

    void changeEmail();

    void changeLogin();

    void changePassword();

    void changeFirstName();

    void changeMiddleName();

    void changeLastName();


}
