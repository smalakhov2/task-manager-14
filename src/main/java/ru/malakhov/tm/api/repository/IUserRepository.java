package ru.malakhov.tm.api.repository;

import ru.malakhov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    String[] profile(String id);

    User changeEmail(String id, String email);

    User changePassword(String id, String password);

    User changeLogin(String id, String login);

    User changeFirstName(String id, String name);

    User changeMiddleName(String id, String name);

    User changeLastName(String id, String name);


}
