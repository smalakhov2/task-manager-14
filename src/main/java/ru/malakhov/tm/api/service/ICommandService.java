package ru.malakhov.tm.api.service;

import ru.malakhov.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}