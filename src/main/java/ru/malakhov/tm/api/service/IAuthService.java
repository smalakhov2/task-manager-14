package ru.malakhov.tm.api.service;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

}
