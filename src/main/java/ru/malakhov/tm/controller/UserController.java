package ru.malakhov.tm.controller;

import ru.malakhov.tm.api.controller.IUserController;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void profile() {
        final String userId = authService.getUserId();
        System.out.println("[PROFILE]");
        String[] profile = userService.profile(userId);
        for (int i = 0; i < profile.length; i++) {
            System.out.println(profile[i]);
        }
    }

    @Override
    public void changeEmail() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE E-MAIL]");
        System.out.println("ENTER NEW E-MAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = userService.changeEmail(userId, email);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeLogin() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = userService.changeLogin(userId, login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changePassword() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        final String currentPassword = TerminalUtil.nextLine();
        if (!HashUtil.salt(currentPassword).equals(userService.findById(userId).getPasswordHash())) {
            throw new EmptyPasswordException();
        }
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        final User user = userService.changePassword(userId, newPassword);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeFirstName() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE FIRST NAME]");
        System.out.println("ENTER NEW FIRST NAME:");
        final String name = TerminalUtil.nextLine();
        final User user = userService.changeFirstName(userId, name);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeMiddleName() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE MIDDLE NAME]");
        System.out.println("ENTER NEW MIDDLE NAME:");
        final String name = TerminalUtil.nextLine();
        final User user = userService.changeMiddleName(userId, name);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeLastName() {
        final String userId = authService.getUserId();
        System.out.println("[CHANGE LAST NAME]");
        System.out.println("ENTER NEW LAST NAME:");
        final String name = TerminalUtil.nextLine();
        final User user = userService.changeLastName(userId, name);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
