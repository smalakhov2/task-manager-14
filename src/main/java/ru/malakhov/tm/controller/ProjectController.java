package ru.malakhov.tm.controller;


import ru.malakhov.tm.api.controller.IProjectController;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(final IProjectService projectService, final IAuthService authService) {
        this.projectService = projectService;
        this.authService = authService;
    }

    private void displayProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void displayProjects() {
        System.out.println("[DISPlAY PROJECTS]");
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authService.getUserId();
        projectService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void displayProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneById(userId,id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void displayProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId,index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void displayProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByName(userId,name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneById(userId,id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(userId,id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId,index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(userId,index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneById(userId,name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneByIndex(userId,index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneByName(userId,name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}