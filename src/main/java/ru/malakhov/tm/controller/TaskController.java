package ru.malakhov.tm.controller;

import ru.malakhov.tm.api.controller.ITaskController;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IAuthService authService;

    public TaskController(final ITaskService taskService, final IAuthService authService) {
        this.taskService = taskService;
        this.authService = authService;
    }

    private void displayTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }


    @Override
    public void displayTasks() {
        final String userId = authService.getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        final String userId = authService.getUserId();
        System.out.println("[CLEAR TASKS]");
        taskService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneById(userId,id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskByIndex() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(userId,index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneByName(userId,name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneById(userId,id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(userId,id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneById(userId,name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneByIndex(userId,index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneByName(userId,name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}