package ru.malakhov.tm.entity;

import java.util.UUID;

public final class Task extends AbstractEntity {

    private String name;

    private String description;

    private String userId;

    private String lastName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public String getLastName() {
        return lastName;
    }
}