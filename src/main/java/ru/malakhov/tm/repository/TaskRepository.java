package ru.malakhov.tm.repository;

import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks){
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (!userId.equals(tasks.get(index).getUserId())) return null;
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}