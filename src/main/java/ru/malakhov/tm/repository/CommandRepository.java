package ru.malakhov.tm.repository;

import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display information about system."
    );

    public static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display arguments."
    );

    public static final Command COMMAND = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, " Close application."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Display task list."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_DISPLAY_BY_ID = new Command(
            TerminalConst.TASK_DISPLAY_BY_ID, null, "Display task by id."
    );

    private static final Command TASK_DISPLAY_BY_NAME = new Command(
            TerminalConst.TASK_DISPLAY_BY_NAME, null, "Display task by name."
    );

    private static final Command TASK_DISPLAY_BY_INDEX = new Command(
            TerminalConst.TASK_DISPLAY_BY_INDEX, null, "Display task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all Projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display task projects."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_DISPLAY_BY_ID = new Command(
            TerminalConst.PROJECT_DISPLAY_BY_ID, null, "Display project by id."
    );

    private static final Command PROJECT_DISPLAY_BY_NAME = new Command(
            TerminalConst.PROJECT_DISPLAY_BY_NAME, null, "Display project by name."
    );

    private static final Command PROJECT_DISPLAY_BY_INDEX = new Command(
            TerminalConst.PROJECT_DISPLAY_BY_INDEX, null, "Display project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    private static final Command LOGIN = new Command(
            TerminalConst.LOGIN, null, "Login."
    );

    private static final Command LOGOUT = new Command(
            TerminalConst.LOGOUT, null, "Logout."
    );

    private static final Command REGISTRY = new Command(
            TerminalConst.REGISTRY, null, "Registry."
    );

    private static final Command PROFILE = new Command(
            TerminalConst.PROFILE, null, "Profile."
    );

    private static final Command CHANGE_EMAIL = new Command(
            TerminalConst.CHANGE_EMAIL, null, "Change e-mail."
    );

    private static final Command CHANGE_LOGIN = new Command(
            TerminalConst.CHANGE_LOGIN, null, "Change login."
    );

    private static final Command CHANGE_PASSWORD = new Command(
            TerminalConst.CHANGE_PASSWORD, null, "Change password."
    );

    private static final Command CHANGE_FIRST_NAME = new Command(
            TerminalConst.CHANGE_FIRST_NAME, null, "Change first name."
    );

    private static final Command CHANGE_LAST_NAME = new Command(
            TerminalConst.CHANGE_LAST_NAME, null, "Change last name."
    );

    private static final Command CHANGE_MIDDLE_NAME = new Command(
            TerminalConst.CHANGE_MIDDLE_NAME, null, "Change middle name."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, ARGUMENT, COMMAND, TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_DISPLAY_BY_ID, TASK_DISPLAY_BY_NAME, TASK_DISPLAY_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_DISPLAY_BY_ID, PROJECT_DISPLAY_BY_NAME, PROJECT_DISPLAY_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX, LOGIN, LOGOUT, REGISTRY, PROFILE, CHANGE_EMAIL,
            CHANGE_LOGIN, CHANGE_PASSWORD, CHANGE_FIRST_NAME, CHANGE_LAST_NAME, CHANGE_MIDDLE_NAME, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}