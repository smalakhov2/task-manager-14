package ru.malakhov.tm.repository;

import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects){
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public Project findOneById(final String userId, String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final String userId, Integer index) {
        if (!userId.equals(projects.get(index).getUserId())) return null;
        return projects.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}