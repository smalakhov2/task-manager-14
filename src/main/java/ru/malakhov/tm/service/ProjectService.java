package ru.malakhov.tm.service;

import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final String userId, final Project project) {
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        projectRepository.clear(userId);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name,  final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}