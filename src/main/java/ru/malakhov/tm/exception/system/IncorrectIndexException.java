package ru.malakhov.tm.exception.system;

import ru.malakhov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}
