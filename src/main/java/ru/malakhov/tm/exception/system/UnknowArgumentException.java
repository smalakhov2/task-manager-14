package ru.malakhov.tm.exception.system;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknowArgumentException extends AbstractException {

    public UnknowArgumentException(final String command) {
        super("Error! Unknown ``" + command + "`` argument...");
    }
}
