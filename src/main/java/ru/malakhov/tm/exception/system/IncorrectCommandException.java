package ru.malakhov.tm.exception.system;

import ru.malakhov.tm.exception.AbstractException;

public class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException() {
        super("Error! Unknow command...");
    }
}
