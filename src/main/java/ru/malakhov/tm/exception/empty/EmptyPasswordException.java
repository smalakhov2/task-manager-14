package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
