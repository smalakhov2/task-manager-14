package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
