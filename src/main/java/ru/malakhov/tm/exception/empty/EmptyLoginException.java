package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
