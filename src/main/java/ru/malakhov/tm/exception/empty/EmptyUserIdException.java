package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! UserId is empty...");
    }

}
