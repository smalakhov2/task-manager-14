package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}
