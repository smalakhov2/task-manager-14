package ru.malakhov.tm.exception;

public abstract class AbstractException extends RuntimeException{

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }
}
